# Glass Aeronautics R-2 Javelin Aerovista #
Exhibition software made in Unity3D for the CGSociety Thrust concept spaceship design competition. Inspired by Forza 4's 'Autovista' feature.

Challenge thread: [Here](http://forums.cgsociety.org/showthread.php?f=433&t=1386219)

Latest dropbox-hosted link: [Here](https://dl.dropboxusercontent.com/u/18297273/R2Aerovista/index.html)

![Exhibition smol.jpg](https://bitbucket.org/repo/9Boaxb/images/4054881874-Exhibition%20smol.jpg)