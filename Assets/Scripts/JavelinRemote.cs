﻿using UnityEngine;
using System.Collections;

public class JavelinRemote : MonoBehaviour {

	public ParticleSystem[] tractors;
	public Animator javAnim;
	public KeyCode gearToggle;
	public KeyCode gearRaise;
	public KeyCode podToggle;
	public AudioClip ding;
	bool isFlying;
	bool isRaised;
	bool hasPods;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(gearToggle) && !isRaised) {
			if (javAnim.GetBool("GearDown")) {
				javAnim.SetBool("GearDown", false);
				isFlying = true;
				foreach (ParticleSystem pS in tractors) {
					pS.Play();
				}
			}
			else {
				javAnim.SetBool("GearDown", true);
				isFlying = false;
				foreach (ParticleSystem pS in tractors) {
					pS.Stop();
				}
			}
			GetComponent<AudioSource>().PlayOneShot(ding);
		}

		if (Input.GetKeyUp(gearRaise) && !isFlying) {
			if (javAnim.GetBool("GearRaise") && !hasPods) {
				javAnim.SetBool("GearRaise", false);
				isRaised = false;
				GetComponent<AudioSource>().PlayOneShot(ding);
			}
			else if (!javAnim.GetBool("GearRaise")) {
				javAnim.SetBool("GearRaise", true);
				isRaised = true;
				GetComponent<AudioSource>().PlayOneShot(ding);
			}
		}

		if (Input.GetKeyUp(podToggle) && !isFlying && isRaised) {
			if (javAnim.GetBool("Pods")) {
				javAnim.SetBool("Pods", false);
				hasPods = false;
			}
			else {
				javAnim.SetBool("Pods", true);
				hasPods = true;
			}
			GetComponent<AudioSource>().PlayOneShot(ding);
		}
	}
}
