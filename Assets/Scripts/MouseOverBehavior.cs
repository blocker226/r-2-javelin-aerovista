﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MouseOverBehavior : MonoBehaviour {

	public string cutsceneClip;
	public AudioClip narrationClip;
	Animator anim;
	AudioSource narration;
	Camera cineCam;
	Camera mainCam;
	Animator camAnim;
	bool isCutScene = false;
	GameObject boxes;

	// Use this for initialization
	void Start () {
		boxes = GameObject.Find("SummaryBoxes");
		mainCam = Camera.main;
		anim = GetComponent<Animator>();
		cineCam = GameObject.FindGameObjectWithTag("CinematicCamera").GetComponent<Camera>();
		camAnim =  cineCam.gameObject.GetComponent<Animator>();
		narration = cineCam.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown && !Input.GetMouseButton(0) && isCutScene) {
//			camAnim.Stop();
			isCutScene = false;
			print("pause?");
			camAnim.SetBool("Exit", true);
			boxes.SetActive(false);
		}
	}

	void LateUpdate () {
		camAnim.SetBool("Exit", false);
	}

	void OnMouseEnter() {
		anim.SetBool("isOpen", true);
	}

	void OnMouseOver() {
		if (Input.GetAxis("Fire1") != 0 && !narration.isPlaying) {
			narration.PlayOneShot(narrationClip);
			if (cutsceneClip != "")
				EnterCutscene(cutsceneClip);
		}
	}

	void OnMouseExit() {
		anim.SetBool("isOpen", false);
	}

	void EnterCutscene(string clip) {
		boxes.SetActive(true);
		cineCam.enabled = true;
		cineCam.GetComponent<AudioListener>().enabled = true;
		mainCam.enabled = false;
		mainCam.GetComponent<AudioListener>().enabled = false;
		isCutScene = true;
			camAnim.Play(clip);
		foreach (GameObject hotspot in GameObject.FindGameObjectsWithTag("Hotspot")) {
			hotspot.SetActive(false);
		}
	}
}
