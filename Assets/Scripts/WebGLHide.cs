﻿using UnityEngine;
using System.Collections;

public class WebGLHide : MonoBehaviour {

	void Awake () {
		if (Application.platform == RuntimePlatform.WebGLPlayer) {
			gameObject.SetActive(false);
		}
	}
}
