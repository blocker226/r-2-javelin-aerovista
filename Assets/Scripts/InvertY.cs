﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class InvertY : MonoBehaviour {

	GameObject FPC;
	bool optionInvertY;

	public void ToggleY (bool option) {
		optionInvertY = option;
		if (GameObject.Find("FPSController")) {
			FPC = GameObject.Find("FPSController");
			FPC.GetComponent<FirstPersonController>().ChangeY(optionInvertY);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (FPC == null) {
			if (GameObject.Find("FPSController")) {
				FPC = GameObject.Find("FPSController");
				FPC.GetComponent<FirstPersonController>().ChangeY(optionInvertY);
			}
		}

	}
}
