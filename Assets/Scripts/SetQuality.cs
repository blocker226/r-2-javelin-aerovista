﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetQuality : MonoBehaviour {

	void Start () {
		GetComponent<Dropdown>().value = QualitySettings.GetQualityLevel();
	}

	public void ChangeQuality (int i) {
		QualitySettings.SetQualityLevel(i);
	}
}
