﻿using UnityEngine;
using System.Collections;

public class AimConstraint : MonoBehaviour {

	public Transform target;
	public Vector3 offset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (target != null) {
			transform.forward = (target.position - transform.position);
			transform.rotation *= Quaternion.Euler(offset);
		}
	}
}
